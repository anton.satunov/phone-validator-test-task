package io.polybius.phonevalidator.validator.util;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PhoneNumberValidationUtilTest {

    @Test
    public void hasInvalidSymbolsShouldReturnTrueOnNull() {
        assertTrue(PhoneNumberValidationUtil.hasInvalidSymbols(null));
    }

    @Test
    public void hasInvalidSymbolsShouldReturnTrueOnEmptyString() {
        assertTrue(PhoneNumberValidationUtil.hasInvalidSymbols(""));
    }

    @Test
    public void hasInvalidSymbolsShouldReturnTrueOnOnlySpaces() {
        assertTrue(PhoneNumberValidationUtil.hasInvalidSymbols("   "));
    }

    @Test
    public void hasInvalidSymbolsShouldReturnTrueOnLetters() {
        assertTrue(PhoneNumberValidationUtil.hasInvalidSymbols("abc"));
    }

    @Test
    public void hasInvalidSymbolsShouldReturnTrueOnSymbols() {
        assertTrue(PhoneNumberValidationUtil.hasInvalidSymbols("+(372)55#5-33-65"));
    }

    @Test
    public void hasInvalidSymbolsShouldReturnTrueOnPlusSignNotInFront() {
        assertTrue(PhoneNumberValidationUtil.hasInvalidSymbols("+(372)55+5-33-65"));
    }

    @Test
    public void hasInvalidSymbolsShouldReturnTrueOnNotClosedBrackets() {
        assertTrue(PhoneNumberValidationUtil.hasInvalidSymbols("+(372 555-33-65"));
    }

    @Test
    public void hasInvalidSymbolsShouldReturnFalseOnBracketsAndNumbersAndSpacesAndDashesWithPlusSignAtStart() {
        assertFalse(PhoneNumberValidationUtil.hasInvalidSymbols("+(372)55 5-33-65"));
    }

    @Test
    public void hasInvalidSymbolsShouldReturnFalseOnOnlyNumbersWithPlusSignAtStart() {
        assertFalse(PhoneNumberValidationUtil.hasInvalidSymbols("+3725553365"));
    }

    @Test
    public void hasInvalidSymbolsShouldReturnFalseOnNumbersAndSpacesWithoutPlusSign() {
        assertFalse(PhoneNumberValidationUtil.hasInvalidSymbols("37255 533 65"));
    }

    @Test
    public void hasInvalidSymbolsShouldReturnFalseOnEmptyBrackets() {
        assertFalse(PhoneNumberValidationUtil.hasInvalidSymbols("+()372 555-33-65"));
    }

    @Test
    public void hasInvalidSymbolsShouldReturnFalseOnBracketsInsideBrackets() {
        assertFalse(PhoneNumberValidationUtil.hasInvalidSymbols("+(3(7)2) 555-33-65"));
    }

}