package io.polybius.phonevalidator.validator.country;

import org.junit.Test;

import static io.polybius.phonevalidator.validator.country.BaseMobilePhoneNumberValidator.CountryConfiguration;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BaseMobilePhoneNumberValidatorTest {

    @Test
    public void isValidPhoneNumberStartShouldReturnTrue() {
        final BaseMobilePhoneNumberValidator validator = new BaseMobilePhoneNumberValidator(CountryConfiguration.EE);
        assertTrue(validator.isValidPhoneNumberStart("37255567634"));
    }

    @Test
    public void isValidCountryCallingCodeShouldReturnTrue() {
        final BaseMobilePhoneNumberValidator validator = new BaseMobilePhoneNumberValidator(CountryConfiguration.EE);
        assertTrue(validator.isValidCountryCallingCode("37255567634"));
    }

    @Test
    public void hasValidLengthShouldReturnTrue() {
        final BaseMobilePhoneNumberValidator validator = new BaseMobilePhoneNumberValidator(CountryConfiguration.EE);
        assertTrue(validator.hasValidLength("37255567634"));
    }

    @Test
    public void isValidPhoneNumberStartShouldReturnFalseOnInvalidPrefix() {
        final BaseMobilePhoneNumberValidator validator = new BaseMobilePhoneNumberValidator(CountryConfiguration.EE);
        assertFalse(validator.isValidPhoneNumberStart("37265567634"));
    }

    @Test
    public void isValidCountryCallingCodeShouldReturnFalseOnInvalidCountryCode() {
        final BaseMobilePhoneNumberValidator validator = new BaseMobilePhoneNumberValidator(CountryConfiguration.EE);
        assertFalse(validator.isValidCountryCallingCode("37155567634"));
    }

    @Test
    public void hasValidLengthShouldReturnFalseOnLengthTooBig() {
        final BaseMobilePhoneNumberValidator validator = new BaseMobilePhoneNumberValidator(CountryConfiguration.EE);
        assertFalse(validator.hasValidLength("372555676343"));
    }

    @Test
    public void hasValidLengthShouldReturnFalseOnLengthTooSmall() {
        final BaseMobilePhoneNumberValidator validator = new BaseMobilePhoneNumberValidator(CountryConfiguration.EE);
        assertFalse(validator.hasValidLength("372555676"));
    }

    @Test
    public void isValidFalseOnLengthTooSmall() {
        final BaseMobilePhoneNumberValidator validator = new BaseMobilePhoneNumberValidator(CountryConfiguration.EE);
        assertFalse(validator.isValid("372555676"));
    }

    @Test
    public void isValidFalseOnInvalidPrefix() {
        final BaseMobilePhoneNumberValidator validator = new BaseMobilePhoneNumberValidator(CountryConfiguration.EE);
        assertFalse(validator.isValid("37265567634"));
    }

    @Test
    public void isValidShouldReturnFalseOnInvalidCountryCode() {
        final BaseMobilePhoneNumberValidator validator = new BaseMobilePhoneNumberValidator(CountryConfiguration.EE);
        assertFalse(validator.isValid("37155567634"));
    }
}