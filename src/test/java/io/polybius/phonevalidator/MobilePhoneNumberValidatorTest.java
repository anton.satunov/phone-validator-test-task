package io.polybius.phonevalidator;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static io.polybius.phonevalidator.validator.country.BaseMobilePhoneNumberValidator.CountryConfiguration;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MobilePhoneNumberValidatorTest {

    private MobilePhoneNumberValidatorService validator;

    @Before
    public void setUp() {
        validator = new MobilePhoneNumberValidatorService();
    }

    @Test
    public void validateLatvianValidPhoneNumber() {
        validateHasOneValidNumber("+37061234567", CountryConfiguration.LV.getIsoCode());
    }

    @Test
    public void validateLithuanianValidPhoneNumber() {
        validateHasOneValidNumber("+37121234567", CountryConfiguration.LT.getIsoCode());
    }

    @Test
    public void validateEstonianValidPhoneNumber() {
        validateHasOneValidNumber("+37251234567", CountryConfiguration.EE.getIsoCode());
    }

    @Test
    public void validateLithuanianInValidPhoneNumberByStart() {
        validateHasOneInvalidPhoneNumber("+37091234567");
    }

    @Test
    public void validateLithuanianInvalidalidPhoneNumberLength() {
        validateHasOneInvalidPhoneNumber("+3706123456");
    }

    @Test
    public void validateBelgianValidPhoneNumber() {
        validateHasOneInvalidPhoneNumber("+3706123456");
    }

    @Test
    public void validateMultipleValidAndInvalidNumbers() {
        final List<String> validEstonianNumbers = List.of(
                "+37253360043", "+3725723515", "+37254355901"
        );
        final List<String> validLatvianNumbers = List.of(
                "+37121607153", "+37125618337", "+37125777601"
        );
        final List<String> validLithuanianNumbers = List.of(
                "+37060270973", "+37068896057", "+37060706601"
        );
        final List<String> invalidNumbers = List.of(
                "+372533600435", "+372572351", "+3725abs4355901",
                "+37121x607153", "+37125##618337", "+371\t25777601",
                "+3706027073", "+370688960575", "0607+06601");

        final ArrayList<String> allNumbersList = new ArrayList<>();
        allNumbersList.addAll(validEstonianNumbers);
        allNumbersList.addAll(validLatvianNumbers);
        allNumbersList.addAll(validLithuanianNumbers);
        allNumbersList.addAll(invalidNumbers);

        ValidationResultDto result = validator.validate(allNumbersList);
        assertTrue(result.getValidPhonesByCountry().get(CountryConfiguration.EE.getIsoCode()).containsAll(validEstonianNumbers));
        assertTrue(result.getValidPhonesByCountry().get(CountryConfiguration.LT.getIsoCode()).containsAll(validLatvianNumbers));
        assertTrue(result.getValidPhonesByCountry().get(CountryConfiguration.LV.getIsoCode()).containsAll(validLithuanianNumbers));
        assertTrue(result.getInvalidPhones().containsAll(invalidNumbers));
    }

    private void validateHasOneValidNumber(String numberToTest, String countryIsoCode) {
        ValidationResultDto result = validator.validate(List.of(numberToTest));
        assertEquals(Set.of(numberToTest), result.getValidPhonesByCountry().get(countryIsoCode));
        assertEquals(new ArrayList<>(), result.getInvalidPhones());
    }

    private void validateHasOneInvalidPhoneNumber(String numberToTest) {
        ValidationResultDto result = validator.validate(List.of(numberToTest));
        assertTrue(result.getValidPhonesByCountry().isEmpty());
        assertEquals(1, result.getInvalidPhones().size());
        assertTrue(result.getInvalidPhones().contains(numberToTest));
    }

}