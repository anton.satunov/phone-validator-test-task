package io.polybius.phonevalidator;

import io.polybius.phonevalidator.validator.country.BaseMobilePhoneNumberValidator;
import io.polybius.phonevalidator.validator.country.CountryMobilePhoneNumberValidator;

import java.util.List;

import static io.polybius.phonevalidator.validator.country.BaseMobilePhoneNumberValidator.CountryConfiguration;
import static io.polybius.phonevalidator.validator.util.PhoneNumberValidationUtil.getNumericRepresentationOrEmptyString;
import static io.polybius.phonevalidator.validator.util.PhoneNumberValidationUtil.hasInvalidSymbols;
import static java.util.Objects.isNull;

public class MobilePhoneNumberValidatorService {

    private final List<CountryMobilePhoneNumberValidator> countryValidators = List.of(
            new BaseMobilePhoneNumberValidator(CountryConfiguration.EE),
            new BaseMobilePhoneNumberValidator(CountryConfiguration.LV),
            new BaseMobilePhoneNumberValidator(CountryConfiguration.LT),
            new BaseMobilePhoneNumberValidator(CountryConfiguration.BE)
    );

    public ValidationResultDto validate(List<String> phoneNumbers) {
        if (isNull(phoneNumbers) || phoneNumbers.isEmpty()) {
            return new ValidationResultDto();
        }

        final ValidationResultDto validationResultDto = new ValidationResultDto();
        phoneNumbers.forEach(phoneNumber -> addPhoneNumberAsValidOrInvalid(phoneNumber, validationResultDto));

        return validationResultDto;
    }

    private void addPhoneNumberAsValidOrInvalid(String phoneNumber, ValidationResultDto validationResultDto) {
        if (hasInvalidSymbols(phoneNumber)) {
            addInvalid(phoneNumber, validationResultDto);
            return;
        }
        final String numericRepresentation = getNumericRepresentationOrEmptyString(phoneNumber);

        countryValidators.stream()
                .filter(validator -> validator.isValid(numericRepresentation))
                .findFirst()
                .ifPresentOrElse(
                        validator -> addValid(validationResultDto, numericRepresentation, validator.getCountryIsoCode()),
                        () -> addInvalid(phoneNumber, validationResultDto)
                );
    }

    private void addValid(ValidationResultDto validationResultDto, String numericRepresentation, String countryIsoCode) {
        validationResultDto.addValidPhoneNumber(countryIsoCode, numericRepresentation);
    }

    private void addInvalid(String phoneNumber, ValidationResultDto validationResultDto) {
        validationResultDto.addInvalidPhoneNumber(phoneNumber);
    }
}
