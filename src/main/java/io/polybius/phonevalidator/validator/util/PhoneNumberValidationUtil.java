package io.polybius.phonevalidator.validator.util;

import java.util.Arrays;
import java.util.List;

import static java.util.Objects.isNull;

public class PhoneNumberValidationUtil {

    private static final List<String> ALLOWED_SPECIAL_CHARACTERS = Arrays.asList(" ", "-", "\\)", "\\(");
    private static final String PLUS_SIGN = "+";
    private static final char CLOSING_BRACKETS_CHAR = ')';
    private static final char OPENING_BRACKETS_CHAR = '(';

    public static String getNumericRepresentationOrEmptyString(String phoneNumber) {
        String phoneNumberWithRemovedCharacters = getPhoneNumberWithoutPlusSignPrefix(phoneNumber);
        for (String allowedCharacter : ALLOWED_SPECIAL_CHARACTERS) {
            phoneNumberWithRemovedCharacters = phoneNumberWithRemovedCharacters.replaceAll(allowedCharacter, "");
        }
        try {
            Long.valueOf(phoneNumberWithRemovedCharacters);
            return phoneNumberWithRemovedCharacters;
        } catch (NumberFormatException e) {
            return "";
        }
    }

    public static boolean hasInvalidSymbols(String phoneNumber) {
        if (isBlank(phoneNumber)) {
            return true;
        }
        return hasNotAllowedCharacters(phoneNumber) || hasInvalidBrackets(phoneNumber);
    }

    private static boolean isBlank(String string) {
        if (isNull(string) || string.length() == 0) {
            return true;
        }
        return string.chars()
                .allMatch(Character::isWhitespace);
    }

    private static boolean hasNotAllowedCharacters(String phoneNumber) {
        return getNumericRepresentationOrEmptyString(phoneNumber).isEmpty();
    }

    private static String getPhoneNumberWithoutPlusSignPrefix(String phoneNumber) {
        String phoneNumberWithRemovedCharacters = phoneNumber;
        if (phoneNumber.startsWith(PLUS_SIGN)) {
            phoneNumberWithRemovedCharacters = phoneNumber.substring(1);
        }
        return phoneNumberWithRemovedCharacters;
    }

    private static boolean hasInvalidBrackets(String phoneNumber) {
        int openBracketsCount = 0;
        for (int i = 0; i < phoneNumber.length(); i++) {
            final char characterAtIndex = phoneNumber.charAt(i);
            if (characterAtIndex == CLOSING_BRACKETS_CHAR && openBracketsCount == 0) {
                return true;
            } else if (characterAtIndex == OPENING_BRACKETS_CHAR) {
                openBracketsCount++;
            } else if (characterAtIndex == CLOSING_BRACKETS_CHAR) {
                openBracketsCount--;
            }
        }

        return openBracketsCount != 0;
    }

}
