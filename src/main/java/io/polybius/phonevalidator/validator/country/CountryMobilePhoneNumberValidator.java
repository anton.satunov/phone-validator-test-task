package io.polybius.phonevalidator.validator.country;

public interface CountryMobilePhoneNumberValidator {

    boolean isValid(String phoneNumberWithoutSpecialCharacters);

    String getCountryIsoCode();

}
