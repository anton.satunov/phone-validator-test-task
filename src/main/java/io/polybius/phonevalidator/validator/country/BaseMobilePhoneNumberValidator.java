package io.polybius.phonevalidator.validator.country;

import java.util.Set;

public class BaseMobilePhoneNumberValidator implements CountryMobilePhoneNumberValidator {

    private final CountryConfiguration country;

    public BaseMobilePhoneNumberValidator(CountryConfiguration country) {
        this.country = country;
    }

    @Override
    public boolean isValid(String phoneNumberWithoutSpecialCharacters) {
        return isValidCountryCallingCode(phoneNumberWithoutSpecialCharacters)
                && hasValidLength(phoneNumberWithoutSpecialCharacters)
                && isValidPhoneNumberStart(phoneNumberWithoutSpecialCharacters);
    }

    @Override
    public String getCountryIsoCode() {
        return country.getIsoCode();
    }

    boolean isValidPhoneNumberStart(String phoneNumberWithoutSpecialCharacters) {
        return country.getAllowedPhoneNumberPrefixes().stream()
                .anyMatch(prefix -> phoneNumberWithoutSpecialCharacters.startsWith(country.getCallingCode() + prefix));
    }

    boolean hasValidLength(String phoneNumberWithoutSpecialCharacters) {
        final int lengthWithoutPrefix = phoneNumberWithoutSpecialCharacters.length() - country.getCallingCode().length();
        return country.getAllowedPhoneNumberLengths().contains(lengthWithoutPrefix);
    }

    boolean isValidCountryCallingCode(String phoneNumberWithoutSpecialCharacters) {
        return phoneNumberWithoutSpecialCharacters.startsWith(country.getCallingCode());
    }

    public enum CountryConfiguration {

        EE("EE", "372", Set.of("5"), Set.of(7, 8)),
        LV("LV", "370", Set.of("6"), Set.of(8)),
        LT("LT", "371", Set.of("2"), Set.of(8)),
        BE("BE", "32", Set.of("456", "47", "48", "49"), Set.of(9));

        private final String isoCode;
        private final String callingCode;
        private final Set<String> allowedPrefixes;
        private final Set<Integer> allowedLengths;

        CountryConfiguration(String isoCode, String callingCode, Set<String> allowedPrefixes, Set<Integer> allowedLengths) {
            this.isoCode = isoCode;
            this.callingCode = callingCode;
            this.allowedPrefixes = allowedPrefixes;
            this.allowedLengths = allowedLengths;
        }

        public String getIsoCode() {
            return isoCode;
        }

        public String getCallingCode() {
            return callingCode;
        }

        public Set<String> getAllowedPhoneNumberPrefixes() {
            return allowedPrefixes;
        }

        public Set<Integer> getAllowedPhoneNumberLengths() {
            return allowedLengths;
        }
    }

}
