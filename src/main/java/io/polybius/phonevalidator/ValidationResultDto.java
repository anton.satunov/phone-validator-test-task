package io.polybius.phonevalidator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ValidationResultDto {
    private final Map<String, Set<String>> validPhonesByCountry = new HashMap<>();
    private final List<String> invalidPhones = new ArrayList<>();

    public Map<String, Set<String>> getValidPhonesByCountry() {
        return new HashMap<>(validPhonesByCountry);
    }

    public List<String> getInvalidPhones() {
        return new ArrayList<>(invalidPhones);
    }

    public void addValidPhoneNumber(String countryCode, String phoneNumber) {
        if (!validPhonesByCountry.containsKey(countryCode)) {
            final Set<String> countryPhoneNumberList = new HashSet<>();
            validPhonesByCountry.put(countryCode, countryPhoneNumberList);
        }
        validPhonesByCountry.get(countryCode).add("+" + phoneNumber);
    }

    public void addInvalidPhoneNumber(String phoneNumber) {
        invalidPhones.add(phoneNumber);
    }
}
